/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.jca;

import javax.naming.NamingException;
import javax.naming.Reference;
import javax.resource.ResourceException;
import javax.resource.cci.Connection;
import javax.resource.cci.ConnectionFactory;
import javax.resource.cci.ConnectionSpec;
import javax.resource.cci.RecordFactory;
import javax.resource.cci.ResourceAdapterMetaData;
import javax.resource.spi.ConnectionManager;

import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.Command;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.Value;

public class AbstractOptimisticXAResourceConnectionFactory<T extends Value<T, C>, C extends Command<T>> implements ConnectionFactory {

	private static final long serialVersionUID = 1L;

	protected final AbstractOptimisticXAResourceManagedConnectionFactory<T, C> managedConnectionFactory;
	protected final ConnectionManager cxManager;
	
	public AbstractOptimisticXAResourceConnectionFactory(ConnectionManager cxManager, AbstractOptimisticXAResourceManagedConnectionFactory<T, C> managedConnectionFactory) {
		this.cxManager = cxManager;
		this.managedConnectionFactory = managedConnectionFactory;
	}
	
	@Override
	public void setReference(Reference reference) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Reference getReference() throws NamingException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Connection getConnection() throws ResourceException {
		return (Connection) cxManager.allocateConnection(managedConnectionFactory, null);
	}
	
	@Override
	public Connection getConnection(ConnectionSpec properties) throws ResourceException {
		return (Connection) cxManager.allocateConnection(managedConnectionFactory, null);
	}

	@Override
	public RecordFactory getRecordFactory() throws ResourceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResourceAdapterMetaData getMetaData() throws ResourceException {
		// TODO Auto-generated method stub
		return null;
	}
}
