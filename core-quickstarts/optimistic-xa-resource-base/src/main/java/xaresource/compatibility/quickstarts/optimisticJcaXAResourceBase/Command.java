/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase;

import java.io.Serializable;
import java.util.function.UnaryOperator;

import com.thoughtworks.xstream.XStream;

public interface Command<T> extends Serializable, UnaryOperator<T> {

	default public String toXML() {
		XStream xstream = new XStream();
		return xstream.toXML(this);
	}
	
}
