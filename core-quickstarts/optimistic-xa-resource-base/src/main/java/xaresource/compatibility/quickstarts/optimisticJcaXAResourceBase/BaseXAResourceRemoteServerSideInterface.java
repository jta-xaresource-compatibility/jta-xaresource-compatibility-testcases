/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase;

import javax.transaction.xa.XAException;
import javax.transaction.xa.XAResource;
import javax.transaction.xa.Xid;

public interface BaseXAResourceRemoteServerSideInterface<T extends Value<T, C>, C extends Command<T>> {


	public void remoteServerSideXAResourceStart(Xid xid, int flags) throws XAException;
	
	public void remoteServerSideXAResourceEnd(Xid xid, int flags) throws XAException;

	public int remoteServerSideXAResourcePrepare(Xid xid) throws XAException;

	public void remoteServerSideXAResourceCommit(Xid xid, boolean onePhase) throws XAException;

	public void remoteServerSideXAResourceRollback(Xid xid) throws XAException;

	// XXX potentially this method does not need to exist here
	public boolean isSameRM(XAResource xares) throws XAException;

	public void forget(Xid xid) throws XAException;

	public Xid[] recover(int flag) throws XAException;
	
	// XXX potentially these two TransactionTimeout methods do not need to exist here
	public int getTransactionTimeout() throws XAException;
	public boolean setTransactionTimeout(int seconds) throws XAException;
	
	// XXX these two TransactionContext methods needs to move into the ManagedConnectionFactory, 
	//     because in a remote context this cannot work.
	public BaseXAResourceTransactionContext<T, C> getTransactionContext(Xid xid);
	public void setTransactionContext(Xid xid, BaseXAResourceTransactionContext<T, C> tc);
	
	public void apply(Xid xid, C command);
	
	public T getImmutableValue(Xid xid);
	
	public boolean isLocked(BaseXAResourceImmutableState<T, C> state);
	
		
}
