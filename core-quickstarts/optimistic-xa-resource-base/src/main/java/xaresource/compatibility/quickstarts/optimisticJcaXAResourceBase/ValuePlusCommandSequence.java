/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase;

import java.util.List;

import javax.transaction.xa.Xid;

import org.javatuples.Quartet;

@SuppressWarnings("rawtypes")
public class ValuePlusCommandSequence<V extends Value, C extends Command<V>> {
	
	private final Quartet<Xid, V, V, List<C>> data;
	
	public ValuePlusCommandSequence(Xid xid, V start, V end, List<C> commandSequence) {
		this.data = new Quartet<>(xid, start, end, commandSequence);
	}

	public Xid getXid() {
		return data.getValue0();
	}
	
	public V getBaseValue() {
		return data.getValue1();
	}
	
	public V getFinalValue() {
		return data.getValue2();
	}
	
	public List<C> getCommandSequence() {
		return data.getValue3();
	}
	
	public Quartet<Xid, V, V, List<C>> getData() {
		return data;
	}

}
