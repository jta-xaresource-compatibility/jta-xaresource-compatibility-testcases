/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.quickstarts.counter.optimisticInMemoryJcaCounter;

import java.util.concurrent.atomic.AtomicInteger;

import javax.resource.ResourceException;
import javax.resource.cci.ConnectionFactory;
import javax.resource.spi.ConnectionManager;
import javax.transaction.xa.XAResource;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import xaresource.compatibility.IsolationLevel;
import xaresource.compatibility.testcases.SupplierConstructorArguments;
import xaresource.compatibility.testcases.adapters.ResourceConnectionFactoryTestAdapter;
import xaresource.compatibility.testcases.adapters.ResourceConnectionTestAdapter;

import com.arjuna.ats.jta.recovery.XAResourceRecoveryHelper;

public class CounterResourceConnectionFactoryTestAdapter implements ResourceConnectionFactoryTestAdapter {

	private final static Logger logger = LoggerFactory.getLogger(CounterResourceConnectionFactoryTestAdapter.class);
	private static final AtomicInteger sequence = new AtomicInteger();
	private final ConnectionFactory connectionFactory;
	
	
	public static ResourceConnectionFactoryTestAdapter create(@NonNull SupplierConstructorArguments supplierConstructorArguments) {
		// if(null == supplierConstructorArguments) supplierConstructorArguments = new SupplierConstructorArguments();
		int nr =  sequence.incrementAndGet();
		String baseName = "myname";
		if(supplierConstructorArguments.idOrConnectionString != null)
			baseName = supplierConstructorArguments.idOrConnectionString;
		String id = baseName + nr;
		logger.info("public static CounterResourceConnectionFactoryTestAdapter create(): '" + id + "'");
		return new CounterResourceConnectionFactoryTestAdapter(id, supplierConstructorArguments.connectionManager);
	}
	
	public CounterResourceConnectionFactoryTestAdapter(String name, ConnectionManager intercept) {
		logger.debug("Constructing CounterResourceConnectionFactoryTestAdapter");
		this.connectionFactory = (new XAResourceCounterNanny()).summon(name, intercept);
	}
	
	protected XAResourceCounterImpl.ManagedConnection getManagedConnection() {
		XAResourceCounterImpl.CciConnection cciConnection = null;
		XAResourceCounterImpl.ManagedConnection result = null;
		try {
			cciConnection = (XAResourceCounterImpl.CciConnection) connectionFactory.getConnection();
			result = cciConnection.getManagedConnection();
		} catch (ResourceException e) {
			logger.error("ResourceException through while trying to get the ManagedConnection", e);
		}
		return result;
	}
	
	@Override
	public ResourceConnectionTestAdapter getResourceConnectionTestAdapter() {
		XAResourceCounterImpl.CciConnection cciConnection = null;
		ResourceConnectionTestAdapter result = null;
		try {
			cciConnection = (XAResourceCounterImpl.CciConnection) connectionFactory.getConnection();
			result = new CounterResourceConnectionTestAdapter(cciConnection.getManagedConnection());
		} catch (ResourceException e) {
			logger.error("ResourceException through while trying to construct the ResourceConnectionTestAdapter", e);
		}
		return result;
	}

	@Override
	public IsolationLevel getIsloationLevel() {
		return this.getManagedConnection().getBaseResource().getIsolationLevel();
	}

	@Override
	public TransactionStartsWith getTransactionStartsWith() {
		return TransactionStartsWith.START;
	}

	@Override
	public boolean isChange2Independent() {
		return false;
	}

	@Override
	public boolean isPessimisticLocking() {
		return false;
	}

	@Override
	public XAResourceRecoveryHelper getXAResourceRecoveryHelper() {
		return new XAResourceRecoveryHelper() {

			@Override
			public boolean initialise(String p) throws Exception {
				return true;
			}

			@Override
			public XAResource[] getXAResources() throws Exception {
				return new XAResource[]{getManagedConnection().getXAResource()};
			}
			
		};
	}

	/*
	protected Counter getCounter() {
		Connection connection = null;
		try {
			connection = connectionFactory.getConnection();
		} catch (ResourceException e) {
			logger.error("ResourceException thrown when trying to perform getCounter()", e);
		}
		Counter counter = (Counter) connection;
		return counter;
	}
	*/

	@Override
	public ResourceConnectionFactoryTestAdapter createFreshWriteToInstance(ConnectionManager intercept) {
		return create(new SupplierConstructorArguments(intercept));
	}
	

}
