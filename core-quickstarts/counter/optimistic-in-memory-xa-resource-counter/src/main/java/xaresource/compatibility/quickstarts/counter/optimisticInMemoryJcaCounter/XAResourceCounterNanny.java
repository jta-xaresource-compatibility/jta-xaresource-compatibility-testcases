/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.quickstarts.counter.optimisticInMemoryJcaCounter;

import javax.resource.ResourceException;
import javax.resource.cci.ConnectionFactory;
import javax.resource.spi.ConnectionManager;
import javax.resource.spi.ManagedConnectionFactory;

import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.jca.ResourceNanny;

public class XAResourceCounterNanny implements ResourceNanny {

	@Override
	public ConnectionFactory summon(String name) {
		return summon(name, null);
	}

	@Override
	public ConnectionFactory summon(String name, ConnectionManager intercept) {
		XAResourceCounterImpl xaResourceCounterImpl = new XAResourceCounterImpl(name);
		ManagedConnectionFactory managedConnectionFactory = xaResourceCounterImpl.getManagedConnectionFactory();
		ConnectionFactory connectionFactory = null;
		try {
			if(null == intercept)
				connectionFactory = (ConnectionFactory) managedConnectionFactory.createConnectionFactory();
			else
				connectionFactory = (ConnectionFactory) managedConnectionFactory.createConnectionFactory(intercept);
		} catch (ResourceException e) {
			throw new RuntimeException("There was an error while trying to create the connection factory!", e);
		}
		return connectionFactory;
	}

}
