/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.quickstarts.counter.optimisticInMemoryJcaCounter.command;

import java.util.List;

import org.javatuples.Triplet;

import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.Command;

public class CounterBaseValuePlusCommandSequence {
	
	private final Triplet<CounterValue, CounterValue, List<Command<CounterValue>>> data;
	
	public CounterBaseValuePlusCommandSequence(CounterValue start, CounterValue end, List<Command<CounterValue>> commandSequence) {
		this.data = new Triplet<>(start, end, commandSequence);
	}
	
	public CounterValue getCounterBase() {
		return data.getValue0();
	}
	
	public CounterValue getCounterFinal() {
		return data.getValue1();
	}
	
	public List<Command<CounterValue>> getCommandSequence() {
		return data.getValue2();
	}
	
	public Triplet<CounterValue, CounterValue, List<Command<CounterValue>>> getData() {
		return data;
	}
}
